import Foundation
import CoreData
import UIKit

class NotificationsFacade {
    var onUpdate: (() -> Void)?
    var onError: (() -> Void)?
    var onNoNetwork: (() -> Void)?
    
    private let model: NotificationsModel
    
    init() {
        self.model = AppModel.instance.notificationsModel
    }
    
    func fetchedResultController() -> NSFetchedResultsController<MyNotification> {
        let predicate = NSPredicate(format: "hidden == %@", NSNumber(booleanLiteral: false))
        //let ex = CoreDataManager.instance.fetchedResultController(entityName: "MyNotification", predicate: predicate, orderBy: "timeStamp")
        return CoreDataManager.instance.fetchedResultController(entityName: "MyNotification", predicate: predicate, orderBy: "timeStamp")
       
    }
    
    func hide(notification: MyNotification) {
        model.hide(notification: notification)
        onUpdateSuccess()
    }
    
    var hasNotifications: Bool {
        return model.notificationsFromStorage().count > 0
    }
    
    var notificationsCount: Int {
        return model.notificationsFromStorage().count
    }
    
    func updateNotifications() {
        model.updateNotificationsInStorage(onError: onError, onSuccess: onUpdateSuccess)
    }
    
    func getNotificationsAboutRouteFlight(routeID : Int64, flightID : Int64) -> [MyNotification]?{
        let fetchedRes = fetchedResultController()
        try? fetchedRes.performFetch()
        let notificationsArray = fetchedRes.fetchedObjects
        var routeFlightsWithNotifications = [MyNotification] ()
        if (notificationsArray != nil){
        for notification in notificationsArray! {
            if (notification.routeID == routeID && notification.flightID == flightID){
                routeFlightsWithNotifications.append(notification)
            }
        }
       return routeFlightsWithNotifications
        } else {return nil}
    }
    
    func onError(error: NetworkError) {
        switch error {
        case .fault(_): onError?()
        case .offline: onNoNetwork?()
        }
    }
    
    func onUpdateSuccess() {
        onUpdate?()
    }
}
