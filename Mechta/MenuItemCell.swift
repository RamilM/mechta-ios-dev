import UIKit

class MenuItemCell: UITableViewCell {
    @IBOutlet var coloredBar: UIView?
    @IBOutlet var iconImageView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var selectionView: UIView?
    
    var menuItem: MenuItem? {
        didSet {
            showData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionView?.layer.cornerRadius = 3;
        selectionView?.layer.masksToBounds = true;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        selectionView?.isHidden = !selected
    }

    func showData() {
        guard menuItem != nil else {
            return
        }
        
        coloredBar?.backgroundColor = menuItem?.color ?? UIColor.clear
        iconImageView?.image = UIImage(named: menuItem!.iconName)
        titleLabel?.text = menuItem?.title
        selectionView?.isHidden = true
    }
}
