//
//  AppDelegateFirebase.swift
//  Mechta
//
//  Created by Evgeniy Safronov on 14.10.17.
//

import UIKit
import UserNotifications
import FirebaseMessaging
import FirebaseCore

// MARK: - Регистрация Firebase

extension AppDelegate {
    
    func configureFirebase(application: UIApplication) {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
    }
}

// MARK: - Обновление токена. Управление подписками.

extension AppDelegate: MessagingDelegate {
    
    // FCM tokens are always provided here. It is called generally during app start, but may be called
    // more than once, if the token is invalidated or updated. This is the right spot to upload this
    // token to your application server, or to subscribe to any topics.
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        //check if app launched first time
        if(UserDefaults.standard.value(forKey:"stock")==nil){
            Messaging.messaging().subscribe(toTopic: "/topics/declined_bus")
            Messaging.messaging().subscribe(toTopic: "/topics/road_situation")
            Messaging.messaging().subscribe(toTopic: "/topics/last_bus")
            Messaging.messaging().subscribe(toTopic: "/topics/common")
            Messaging.messaging().subscribe(toTopic: "/topics/stock")
        } else {
            //set all according to sharedPreferences
            if(UserDefaults.standard.value(forKey:"declined_bus") as! Bool == true){
               Messaging.messaging().subscribe(toTopic: "/topics/declined_bus")
            } else {
                Messaging.messaging().unsubscribe(fromTopic: "/topics/declined_bus")
            }
            
            if(UserDefaults.standard.value(forKey:"road_situation") as! Bool == true){
                Messaging.messaging().subscribe(toTopic: "/topics/road_situation")
            } else {
                Messaging.messaging().unsubscribe(fromTopic: "/topics/road_situation")
            }
            
            if(UserDefaults.standard.value(forKey:"last_bus") as! Bool == true){
                Messaging.messaging().subscribe(toTopic: "/topics/last_bus")
            } else {
                Messaging.messaging().unsubscribe(fromTopic: "/topics/last_bus")
            }
            
            if(UserDefaults.standard.value(forKey:"common") as! Bool == true){
                Messaging.messaging().subscribe(toTopic: "/topics/common")
            } else {
                Messaging.messaging().unsubscribe(fromTopic: "/topics/common")
            }
            
            if(UserDefaults.standard.value(forKey:"stock") as! Bool == true){
                Messaging.messaging().subscribe(toTopic: "/topics/stock")
            } else {
                Messaging.messaging().unsubscribe(fromTopic: "/topics/stock")
            }
        }
       
        if let token = Messaging.messaging().fcmToken {
            print("FCM Token: \(token)")
        } else {
            print("FCM Token: nil")
        }
    }
}

// MARK: - Получение уведомлений на iOS 8-9

extension AppDelegate {
    
    // Сообщение получено
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print("Пришло уведомление: \(userInfo)")
        
        
        let application = UIApplication.shared
        if application.applicationState == .active {
            let localNotification = UILocalNotification()
            if let aps = userInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? String {
                    localNotification.userInfo = userInfo
                    localNotification.alertBody = alert
                    localNotification.soundName = UILocalNotificationDefaultSoundName;
                    localNotification.fireDate = Date()
                    print("alert: \(alert)")
                    application.scheduleLocalNotification(localNotification)
                }
            }
        }

    }
    
    // Пользователь отреагировал на сообщение
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PushActionHandler.instance.register(action: PushAction.showNotifications)
        completionHandler(.noData)
    }
}

// MARK: - Получение уведомлений на iOS 10+

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // Сообщение получено
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        //Прибавить счётчик уведомлений
        UIApplication.shared.applicationIconBadgeNumber += 1
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
    }
    
    // Пользователь отреагировал на сообщение
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        PushActionHandler.instance.register(action: PushAction.showNotifications)
        completionHandler()
    }
}
