//
//  CommercialAd+CoreDataProperties.swift
//  
//
//  Created by Ramil Minibaev on 03/09/2017.
//
//
import Foundation
import CoreData


extension CommercialAd {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CommercialAd> {
        return NSFetchRequest<CommercialAd>(entityName: "CommercialAd");
    }
    
    @NSManaged public var detailUrl: String?
    @NSManaged public var id: Int64
    @NSManaged public var itemDescription: String?
    @NSManaged public var photo: String?
    @NSManaged public var publishedAt: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var viewed: Bool
    @NSManaged public var phone: String?
    @NSManaged public var body: String?
    @NSManaged public var priority: String?
    
}
