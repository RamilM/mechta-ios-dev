import UIKit

class ServiceCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    //@IBOutlet weak var dateLabel: UILabel!
    //@IBOutlet weak var detailsButton: UIButton?
    @IBOutlet weak var photoImageView: UIImageView?
    
    private weak var service: Service?
    
    func show(_ service: Service) {
        self.service = service
        
        titleLabel.text = service.title
        descriptionLabel.text = service.phone
        
        photoImageView?.load(
            service.photoUrl,
            loadingPlaceholderName: "ImageLoadingPlaceholder",
            errorPlaceholderName: "ImageErrorPlaceholder"
        )
    }
    
    
    @IBAction func onDetailsButtonClick(_ sender: Any) {
        if let service = service {
           ServicesFacade().showDetailsPage(service: service)
           }
    }
    
    @IBAction func onCallButtonClick(_ sender: Any) {
        if let service = service {
            ServicesFacade().callService(number: service.phone!)
        }
    }
}
