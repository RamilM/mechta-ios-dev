//
//  NewsViewModel.swift
//  Mechta
//
//  Created by Evgeniy Safronov on 27.12.17.
//

class NewsListState {
    var expandedRows = Set<News>()
    
    func expand(item: News) {
        expandedRows.insert(item)
    }
    
    func collapse(item: News) {
        expandedRows.remove(item)
    }
    
    func invertTruncation(item: News) {
        if isExpanded(item: item) {
            collapse(item: item)
        } else {
            expand(item: item)
        }
    }
    
    func isExpanded(item: News) -> Bool {
        return expandedRows.contains(item)
    }
}
