import UIKit

class SideMenuItemsViewController: UITableViewController {
    weak var coordinatorController: SideMenuCoordinatorViewController?
    
    private var items = [MenuItem]()
    private var selectedIndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        items.append(MenuItem(title: "Новости", iconName: "MenuNews", storyboard: "News", startId: "NewsPaging", color: #colorLiteral(red: 0.9629455209, green: 0.8276898265, blue: 0.204836905, alpha: 1)))
        items.append(MenuItem(title: "Транспорт", iconName: "MenuTransport", storyboard: "Transport", startId: "Transport", color: #colorLiteral(red: 0.8865893483, green: 0.1245553568, blue: 0.1383675039, alpha: 1)))
        items.append(MenuItem(title: "Услуги", iconName: "MenuServices", storyboard: "Services", startId: "CommercialAds", color: #colorLiteral(red: 0.6802649498, green: 0.2942598462, blue: 0.5253971219, alpha: 1)))
        items.append(MenuItem(title: "Справочник", iconName: "MenuServices", storyboard: "Services", startId: "Services", color: #colorLiteral(red: 0.7517209053, green: 0.8308458924, blue: 0.5240321159, alpha: 1)))
        items.append(MenuItem(title: "Уведомления", iconName: "MenuNotifications", storyboard: "Notifications", startId: "Notifications", color: #colorLiteral(red: 0.9629455209, green: 0.8276898265, blue: 0.204836905, alpha: 1)))
        items.append(MenuItem(title: "Настройки", iconName: "MenuSettings", storyboard: "Settings", startId: "Settings", color: #colorLiteral(red: 0.6365734339, green: 0.8498307467, blue: 0.9727090001, alpha: 1)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.selectRow(at: selectedIndexPath, animated: false, scrollPosition: .none)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuItemCell
        cell.menuItem = items[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showItem(row: indexPath.row)
    }
    
    private func showItem(row: Int) {
        selectedIndexPath = IndexPath(row: row, section: 0)
        tableView.selectRow(at: selectedIndexPath, animated: false, scrollPosition: .none)
        
        let item = items[row]
        let storyboard = UIStoryboard.init(name: item.storyboard, bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: item.startId)
        coordinatorController?.showViewController(controller)
    }
    
    func showFirstItem() {
        showItem(row: 0)
        PushActionHandler.instance.unregister()
    }
    
    func showNotificationsItem() {
        showItem(row: 4)
        PushActionHandler.instance.unregister()
    }
}
