import UIKit
import YandexMapView

class TransportMapViewController: UIViewController {
    @IBOutlet weak var mapView: YandexMapWebKitView!
    
    private let model = TransportMapFacade()
    private let presetBusStop = "islands#blueMassTransitCircleIcon"
    private let presetBusStopFirst = "islands#nightMassTransitCircleIcon"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.onMapLoaded = onMapLoaded
        mapView.start()
    }
    
    func onMapLoaded() {
        updateData()
        model.onUpdate = updateData
    }
    
    func updateData() {
        mapView.clear()
        
        let busStops = model.busStops
        
        for busStop in busStops {
            mapView.showMarker(id: Int(busStop.id),
                               latitude: busStop.latitude,
                               longitude: busStop.longitude,
                               baloonBody: busStop.title ?? "Остановка",
                               preset: busStop.id == busStops.first?.id ? presetBusStopFirst : presetBusStop)
        }
        
        // Если указаны начальная и конечная остановка - масштабируем карту так чтобы они обе попадали на экран
        if let startBusStop = busStops.first, let endBusStop = busStops.last {
            let nwLat = startBusStop.latitude < endBusStop.latitude ? startBusStop.latitude : endBusStop.latitude
            let seLat = startBusStop.latitude > endBusStop.latitude ? startBusStop.latitude : endBusStop.latitude
            let nwLong = startBusStop.longitude < endBusStop.longitude ? startBusStop.longitude : endBusStop.longitude
            let seLong = startBusStop.longitude > endBusStop.longitude ? startBusStop.longitude : endBusStop.longitude
            
            // Отступ, чтобы маркеры не прилипали к краям экрана
            let padding = 0.005
            
            mapView.setBounds(northWestLat: nwLat-padding, northWestLong: nwLong-padding, southEastLat: seLat+padding, southEastLong: seLong+padding)
        }
        // Если указана начальная остановка - зумимся на ней
        else if let startBusStop = busStops.first {
            mapView.setZoom(zoom: 16)
            mapView.setCenter(latitude: startBusStop.latitude, longitude: startBusStop.longitude)
        }
    }
}
