import Foundation
import CoreData
import CoreDataBoilerplate

class CoreDataManager {
    static let instance = CoreDataManager()
    
    var mainContext: NSManagedObjectContext
    
    private init() {
        let dbStoreName = "Store \(Constants.databaseVersion)"
        mainContext = try! NSManagedObjectContext(modelName: "Model", dbStoreName: dbStoreName, concurrencyType: .mainQueueConcurrencyType)
        
        cleanUp()
    }
    
    private func cleanUp() {
        let fileManager = FileManager.default
        let dirPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
        let dirContent = try! fileManager.contentsOfDirectory(atPath: dirPath)
        
        for fileName in dirContent {
            // Отсекаем только файлы нужного формата
            guard fileName.contains(".sqlite") || fileName.contains(".sqlite-wal") || fileName.contains(".sqlite-shm") else {
                continue
            }
            
            // Не трогаем файлы текущей версии
            guard !fileName.contains("Store \(Constants.databaseVersion)") else {
                continue
            }
            
            let path = dirPath.appending("/").appending(fileName)
            try? fileManager.removeItem(atPath: path)
            
            print("deleted file \(path)")
        }
    }
    
    func concurrentContext() -> NSManagedObjectContext {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = mainContext
        return context
    }
    
    func fetchedResultController<T>(entityName: String, predicate: NSPredicate? = nil, orderBy: String, ascending: Bool = false) -> NSFetchedResultsController<T> {
        return mainContext.fetchedController(entityName: entityName, predicate: predicate, orderBy: orderBy, ascending: ascending)
    }
    
    func fetch<T: NSFetchRequestResult>(_ entityName: String, predicate: NSPredicate? = nil, from context: NSManagedObjectContext? = nil) -> [T] {
        let request = NSFetchRequest<T>(entityName: entityName)
        if predicate != nil {
            request.predicate = predicate
        }
        return (try? (context ?? mainContext).fetch(request)) ?? []
    }
}
