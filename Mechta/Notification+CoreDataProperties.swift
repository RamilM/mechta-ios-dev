//
//  Notification+CoreDataProperties.swift
//  
//
//  Created by Evgeniy Safronov on 16.02.17.
//
//

import Foundation
import CoreData


extension MyNotification {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyNotification> {
        return NSFetchRequest<MyNotification>(entityName: "MyNotification");
    }

    @NSManaged public var hidden: Bool
    @NSManaged public var id: Int64
    @NSManaged public var text: String?
    @NSManaged public var timeStamp: NSDate?
    @NSManaged public var typeRaw: String?
    @NSManaged public var flightID: Int64
    @NSManaged public var routeID: Int64
}
