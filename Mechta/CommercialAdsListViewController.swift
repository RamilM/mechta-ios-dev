//
//  CommercialAdsListViewController.swift
//  Mechta
//
//  Created by Ramil Minibaev on 30/08/2017.
//
//

import UIKit
import CoreData

class CommercialAdsListViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    private var model: CommercialAdsFacade!
    private var fetchedResultController: NSFetchedResultsController<CommercialAd>?
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        hideEmptyRows()
        self.navigationItem.showLogo()
        title="Коммерческие услуги"
        tableView.estimatedRowHeight = 350
        tableView.rowHeight = UITableViewAutomaticDimension
        let action = #selector(CommercialAdsListViewController.reload)
        refreshControl?.addTarget(self, action: action, for: .valueChanged)
        
        model = CommercialAdsFacade()
        model.onNoNetwork = onNoNetworkUpdateError
        model.onUpdate = onDataUpdated
        model.onError = onUpdateError
        
        model.updateCommercialAds()
        
        fetchedResultController = model.fetchedResultController()
        fetchedResultController?.delegate = self
        try? fetchedResultController?.performFetch()
    }
    
    
    func reload(_ sender: Any) {
        model.updateCommercialAds()
    }
    
    func onDataUpdated() {
        refreshControl?.endRefreshing()
        
        if model.hasCommercialAds {
            showContentBackground()
        } else {
            showMessageBackground("Услуг нет", subtitle: "Потяните экран, чтобы обновить")
        }
    }
    
    func onUpdateError() {
        if model.hasCommercialAds {
            showMessageAlert("Не удалось загрузить данные") { [weak self] in
                self?.refreshControl?.endRefreshing()
            }
        } else {
            refreshControl?.endRefreshing()
            showMessageBackground("Ошибка", subtitle: "Не удалось загрузить новости")
        }
    }
    
    func onNoNetworkUpdateError() {
        if model.hasCommercialAds {
            showMessageAlert("Отсутствует подключение к интернету") {  [weak self] in
                self?.refreshControl?.endRefreshing()
            }
        } else {
            refreshControl?.endRefreshing()
            showMessageBackground("Ошибка", subtitle: "Отсутствует подключение к интернету")
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultController?.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultController!.sections![section].numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let commercialAd = fetchedResultController!.object(at: indexPath)
        
        var cellId:String
        let havePhotoHaveURL = (commercialAd.photoUrl != nil ? true : false, commercialAd.detailUrl != nil ? true : false)
        switch havePhotoHaveURL {
        case (false, true):
            cellId = "CommercialAdCellNoPhoto"
        case (true, false):
            cellId = "CommercialAdCellNoURL"
        case (true, true):
            cellId = "CommercialAdCell"
        default:
            cellId = "CommercialAdCellNoPhotoNoURL"
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! CommercialAdCell
        cell.show(commercialAd)
        return cell
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                let commercialAd = fetchedResultController!.object(at: indexPath)
                let cell = tableView.cellForRow(at: indexPath) as! CommercialAdCell
                cell.show(commercialAd)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
