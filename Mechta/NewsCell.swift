import UIKit
import TTTAttributedLabel

class NewsCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView?
    @IBOutlet weak var continueReadingButton: UIButton!
    @IBOutlet weak var descriptionLabel: TTTAttributedLabel!
    
    private weak var news: News?
    
    var containsExpandableLabel = false
    
    func show(_ news: News, listState: NewsListState) {
        self.news = news
        titleLabel.text = news.title
        
        let isExpanded = listState.isExpanded(item: news)
        showDescription(news.itemDescription!, expanded: isExpanded)
        
        dateLabel.text = news.publishedAt?.dmyValue()
        continueReadingButton.isEnabled = news.detailUrl != nil
        
        photoImageView?.load(
            news.photoUrl,
            loadingPlaceholderName: "ImageLoadingPlaceholder",
            errorPlaceholderName: "ImageErrorPlaceholder"
        )
    }
    
    private func showDescription(_ description: String, expanded: Bool) {
        let truncatedLinesCount = 5
        let largeLinesCount = 1000 // Требуется, чтобы обрулить баг TTTAttributedLabel
        
        descriptionLabel.text = description
        
        // Делаем так, чтобы точно поместился весь текст и считаем строки
        descriptionLabel.numberOfLines = largeLinesCount
        let requiredNumberOfLines = descriptionLabel.numberOfVisibleLines
        let isExpandableLabel = requiredNumberOfLines > truncatedLinesCount
        
        guard isExpandableLabel else {
            containsExpandableLabel = false
            return
        }
        
        containsExpandableLabel = true
        
        descriptionLabel.numberOfLines = expanded ? largeLinesCount : truncatedLinesCount
        descriptionLabel.attributedTruncationToken = NSAttributedString(string: "...Развернуть", attributes: [
            NSForegroundColorAttributeName: Style.brightColor,
            NSFontAttributeName: UIFont.systemFont(ofSize: 14)
            ])
        
        if expanded {
            let attributedDescription = NSMutableAttributedString(attributedString: descriptionLabel.attributedText)
            let collapseText = NSAttributedString(string: "\nСвернуть", attributes: [
                NSForegroundColorAttributeName: UIColor.red,
                NSFontAttributeName: UIFont.systemFont(ofSize: 14)
                ])
            attributedDescription.append(collapseText)
            descriptionLabel.attributedText = attributedDescription
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView?.image = nil
    }
    
    @IBAction func onContinueReadingButtonClick(_ sender: Any) {
        if news != nil {
            NewsFacade().showDetailsPage(news: news!)
        }
    }
}

extension UILabel {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font.pointSize))
        return rHeight / charSize - 1
    }
}
