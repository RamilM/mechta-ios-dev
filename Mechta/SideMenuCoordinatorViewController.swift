import Foundation
import RESideMenu

class SideMenuCoordinatorViewController: RESideMenu, RESideMenuDelegate {
    fileprivate static let enableMenuGestureHackNotification = NSNotification.Name("SideMenuCoordinatorViewController.EnableMenuGestureHack")
    fileprivate static let disableMenuGestureHackNotification = NSNotification.Name("SideMenuCoordinatorViewController.DisableMenuGestureHack")
    
    private var menuControllerView: UIView?
    
    private var isMenuGestureHackEnabled = false
    private var isMenuDisplaying = false
    
    private var isNotificationsPageDisplayNeeded = false
    private var isInitialPageDisplayNeeded = true
    
    override func awakeFromNib() {
        let sideMenuItemsController = storyboard?.instantiateViewController(withIdentifier: "NavigationController") as! SideMenuItemsViewController
        sideMenuItemsController.coordinatorController = self
        menuControllerView = sideMenuItemsController.view
        leftMenuViewController = sideMenuItemsController
        contentViewController = storyboard?.instantiateViewController(withIdentifier: "ContentController")
        
        delegate = self
        
        scaleMenuView = false
        contentViewShadowEnabled = true
        menuPrefersStatusBarHidden = true
        parallaxEnabled = false
        panFromEdge = true
        panGestureEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if PushActionHandler.instance.lastAction == .showNotifications {
            isNotificationsPageDisplayNeeded = true
            PushActionHandler.instance.unregister()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(enableMenuGestureHack), name: SideMenuCoordinatorViewController.enableMenuGestureHackNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disableMenuGestureHack), name: SideMenuCoordinatorViewController.disableMenuGestureHackNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNotificationsItem), name: PushActionHandler.pushReceivedNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isNotificationsPageDisplayNeeded {
            showNotificationsItem()
            isNotificationsPageDisplayNeeded = false
            isInitialPageDisplayNeeded = false
        }
        else if isInitialPageDisplayNeeded {
            (leftMenuViewController as! SideMenuItemsViewController).showFirstItem()
            isInitialPageDisplayNeeded = false
        }
    }
    
    @objc private func showNotificationsItem() {
        (leftMenuViewController as! SideMenuItemsViewController).showNotificationsItem()
    }
    
    func showViewController(_ controller: UIViewController) {
        let navigationController = contentViewController as! UINavigationController
        navigationController.setViewControllers([controller], animated: false)
        controller.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Sidebar"),
                                                                      style: .plain,
                                                                      target: self,
                                                                      action: #selector(showMenuItems))
        hideViewController()
    }
    
    func showMenuItems() {
        presentLeftMenuViewController()
    }
    
    // MARK: - Включаем/выключаем показ бокового меню из горизонтальных ScrollView
    
    func sideMenu(_ sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        self.isMenuDisplaying = true
    }
    
    func sideMenu(_ sideMenu: RESideMenu!, didHideMenuViewController menuViewController: UIViewController!) {
        self.isMenuDisplaying = false
    }
    
    @objc private func enableMenuGestureHack() {
        isMenuGestureHackEnabled = true
    }
    
    @objc private func disableMenuGestureHack() {
        isMenuGestureHackEnabled = false
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if isMenuGestureHackEnabled && !isMenuDisplaying {
            return true
        } else {
            return false
        }
    }
}

extension UIViewController {
    // ВНИМАНИЕ!
    // Если включить, то свайп с левого края будет показывать боковое меню.
    // Может вызывать конфликты с другими GestureRecognizers
    
    func enableSideMenuGestureHack() {
        NotificationCenter.default.post(name: SideMenuCoordinatorViewController.enableMenuGestureHackNotification, object: nil)
    }
    
    func disableSideMenuGestureHack() {
        NotificationCenter.default.post(name: SideMenuCoordinatorViewController.disableMenuGestureHackNotification, object: nil)
    }
}
