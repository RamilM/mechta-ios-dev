//
//  SettingsViewController.swift
//  Mechta
//
//  Created by Ramil Minibaev on 13/09/2017.
//
//
import UIKit
import FirebaseMessaging
import UserNotifications
class SettingsViewController: UITableViewController{
    @IBOutlet weak var busCancellationSwitch: UISwitch!
    @IBOutlet weak var roadChangesSwitch: UISwitch!
    @IBOutlet weak var lastBusSwitch: UISwitch!
    @IBOutlet weak var commonSwitch: UISwitch!
    @IBOutlet weak var promoSwitch: UISwitch!
    
    @IBOutlet var settingsTable: UITableView!
    
    override func viewDidLoad() {
        hideEmptyRows()
        self.navigationItem.showLogo()
        UserDefaults.standard.synchronize()

        busCancellationSwitch.setOn(UserDefaults.standard.value(forKey: "declined_bus") != nil ? UserDefaults.standard.value(forKey: "declined_bus") as! Bool : true, animated: false)
        roadChangesSwitch.setOn(UserDefaults.standard.value(forKey: "road_situation") != nil ? UserDefaults.standard.value(forKey: "road_situation") as! Bool : true, animated: false)
        lastBusSwitch.setOn(UserDefaults.standard.value(forKey: "last_bus") != nil ? UserDefaults.standard.value(forKey: "last_bus") as! Bool : true, animated: false)
        commonSwitch.setOn(UserDefaults.standard.value(forKey: "common") != nil ? UserDefaults.standard.value(forKey: "common") as! Bool : true, animated: false)
        promoSwitch.setOn(UserDefaults.standard.value(forKey: "stock") != nil ? UserDefaults.standard.value(forKey: "stock") as! Bool : true, animated: false)
    }
    
    @IBAction func busCancellationSwitchPressed(_ sender: UISwitch) {
        if busCancellationSwitch.isOn{
            Messaging.messaging().subscribe(toTopic: "/topics/declined_bus")
            UserDefaults.standard.set(true, forKey: "declined_bus")
            UserDefaults.standard.synchronize()
            }
        else {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/declined_bus")
            UserDefaults.standard.set(false, forKey: "declined_bus")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func roadChangeSwitchPressed(_ sender: UISwitch) {
        if roadChangesSwitch.isOn{
            Messaging.messaging().subscribe(toTopic: "/topics/road_situation")
            UserDefaults.standard.set(true, forKey: "road_situation")
            UserDefaults.standard.synchronize()
        }
        else {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/road_situation")
            UserDefaults.standard.set(false, forKey: "road_situation")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func lastBusSwitchPressed(_ sender: UISwitch) {
        if lastBusSwitch.isOn{
            Messaging.messaging().subscribe(toTopic: "/topics/last_bus")
            UserDefaults.standard.set(true, forKey: "last_bus")
            UserDefaults.standard.synchronize()
        }
        else {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/last_bus")
            UserDefaults.standard.set(false, forKey: "last_bus")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func commonSwitchPressed(_ sender: UISwitch) {
        if commonSwitch.isOn{
            Messaging.messaging().subscribe(toTopic: "/topics/common")
            UserDefaults.standard.set(true, forKey: "common")
            UserDefaults.standard.synchronize()
        }
        else {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/common")
            UserDefaults.standard.set(false, forKey: "common")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func promoSwitchPressed(_ sender: UISwitch) {
        if promoSwitch.isOn{
            Messaging.messaging().subscribe(toTopic: "/topics/stock")
            UserDefaults.standard.set(true, forKey: "stock")
            UserDefaults.standard.synchronize()
        }
        else {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/stock")
            UserDefaults.standard.set(false, forKey: "stock")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    
}

