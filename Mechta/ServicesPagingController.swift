import UIKit

class ServicesPagingController: PagingViewControoler {
    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Services", bundle: Bundle.main)
        
        let servicesController = storyboard.instantiateViewController(withIdentifier: "Services") as! ServiceListViewController
        let commercialAdsController = storyboard.instantiateViewController(withIdentifier: "CommercialAds") as! CommercialAdsListViewController
        
        showItems([ ("Коммерческие", commercialAdsController),("Муниципальные", servicesController)])
    }
}
