//
//  TransportMapContainerViewController.swift
//  Mechta
//
//  Created by Evgeniy Safronov on 03.11.17.
//

import UIKit

class TransportMapContainerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = nil
    }
    
    @IBAction func onDoneButtonClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
