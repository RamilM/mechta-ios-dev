//
//  CommercialAdsFacade.swift
//  Mechta
//
//  Created by Ramil Minibaev on 30/08/2017.
//
//

import Foundation
import CoreData
import UIKit

class CommercialAdsFacade {
    var onUpdate: (() -> Void)?
    var onError: (() -> Void)?
    var onNoNetwork: (() -> Void)?
    
    private let model: CommercialAdsModel
    
    init() {
        self.model = AppModel.instance.commercialAdsModel
    }
    
    func fetchedResultController() -> NSFetchedResultsController<CommercialAd> {
        return fetchedResultControllerAll()
    }
    
    private func fetchedResultControllerAll() -> NSFetchedResultsController<CommercialAd> {
        return CoreDataManager.instance.fetchedResultController(entityName: "CommercialAd", orderBy: "priority")
    }
    
    func showDetailsPage(commercialAd: CommercialAd) {
        guard let detailUrl = commercialAd.detailUrl else {
            return
        }
        
        if let url = URL(string: detailUrl) {
            UIApplication.shared.openURL(url)
        }
        
        model.markViewed(commercialAd)
    }
    
    func callService(number: String) {
        guard let url = URL(string: "telprompt://"+number.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")) else {return}
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    var hasCommercialAds: Bool {
        return model.commercialAdsFromStorage().count > 0
    }
    
    var unviewedCommercialAdsCount: Int {
        return model.unviewedcommercialAdsFromStorage().count
    }
    
    func updateCommercialAds() {
        model.updateCommercialAdsInStorage(onError: onError, onSuccess: onUpdateSuccess)
    }
    
    func onError(error: NetworkError) {
        switch error {
        case .fault(_): onError?()
        case .offline: onNoNetwork?()
        }
    }
    
    func onUpdateSuccess() {
        onUpdate?()
    }
}
