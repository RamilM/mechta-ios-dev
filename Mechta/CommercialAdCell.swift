//
//  CommercialAdCell.swift
//  Mechta
//
//  Created by Ramil Minibaev on 30/08/2017.
//
//

import UIKit

class CommercialAdCell: UITableViewCell {
   
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton?
    
    private weak var commercialAd: CommercialAd?
    
    func show(_ commercialAd: CommercialAd) {
        self.commercialAd = commercialAd
        
        titleLabel.text =  commercialAd.title
        
        descriptionLabel.text = commercialAd.phone
            }
    
    
    @IBAction func onDetailsButtonClick(_ sender: Any) {
        if let commercialAd = commercialAd {
            CommercialAdsFacade().showDetailsPage(commercialAd: commercialAd)
        }
    }
    
    @IBAction func  onCallButtonClick(_ sender: Any) {
        if let commercialAd = commercialAd {
            CommercialAdsFacade().callService(number: commercialAd.phone!)
        }
    }
}
