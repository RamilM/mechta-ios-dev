//
//  CommercialAdsModel.swift
//  Mechta
//
//  Created by Ramil Minibaev on 30/08/2017.
//
//

import Foundation
import CoreData

class CommercialAdsModel {
    let mainContext: NSManagedObjectContext
    let networkManager = NetworkManager()
    
    init(context: NSManagedObjectContext) {
        self.mainContext = context
    }
    
    func updateCommercialAdsInStorage(onError: @escaping (NetworkError) -> Void, onSuccess: @escaping () -> Void) {
        networkManager.updateItemsInStorage(
            serviceMethod: "/commercial_ads",
            itemParser: CommercialAd.from,
            entityName: "CommercialAd",
            comparator: {$0 == $1},
            onError: onError,
            onSuccess: onSuccess)
    }
    
    func commercialAdsFromStorage() -> [CommercialAd] {
        return CoreDataManager.instance.fetch("CommercialAd")
    }
    
    
    func unviewedcommercialAdsFromStorage() -> [CommercialAd] {
        let predicate = NSPredicate(format: "viewed == %d", NSNumber(booleanLiteral: false))
        return CoreDataManager.instance.fetch("CommercialAd", predicate: predicate)
    }
    
    func markViewed(_ commercialAd: CommercialAd) {
        commercialAd.viewed = true
        try? commercialAd.managedObjectContext?.saveIfNeeded()
        try? mainContext.saveIfNeeded()
    }
}
