import UIKit

struct MenuItem {
    let title: String
    let iconName: String
    let storyboard: String
    let startId: String
    let color: UIColor
}
