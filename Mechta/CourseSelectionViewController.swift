import UIKit
import CoreData

class CourseSelectionViewController: UIViewController {
    
    @IBOutlet weak var swapDirectionsImageView: UIImageView!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    private let model = RouteSelectionFacade()
    private let busModel = BusStopsFacade()
    private var fetchedResultController: NSFetchedResultsController<BusStop>?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let startTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onStartClick))
        startLabel.isUserInteractionEnabled = true
        startLabel.addGestureRecognizer(startTapRecognizer)
        
        let endTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onEndClick))
        endLabel.isUserInteractionEnabled = true
        endLabel.addGestureRecognizer(endTapRecognizer)
        
        let swapTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(swapBusStops))
        swapDirectionsImageView.isUserInteractionEnabled = true
        swapDirectionsImageView.addGestureRecognizer(swapTapRecognizer)
        
        model.onUpdate = updateData
        updateData()
        
        busModel.updateBusStops()
        
        fetchedResultController = busModel.fetchedResultController()
        fetchedResultController?.delegate = self as? NSFetchedResultsControllerDelegate
        try? fetchedResultController?.performFetch()
        for busStop in (fetchedResultController?.fetchedObjects)!{
            if (busStop.id==5) {model.startBusStop=busStop}
            if (busStop.id==10) {model.endBusStop=busStop}
        }
    }
    
    func onStartClick() {
        if let controller = storyboard?.instantiateViewController(withIdentifier: "BusStops") as? BusStopListViewController {
            navigationController?.pushViewController(controller, animated: true)
            controller.title = "Начало пути"
            controller.onBusStopSelected = {
                [weak self] busStop in
                if self?.model.endBusStop == busStop {
                    self?.swapBusStops()
                } else {
                    self?.model.startBusStop = busStop
                }
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func onEndClick() {
        if let controller = storyboard?.instantiateViewController(withIdentifier: "BusStops") as? BusStopListViewController {
            navigationController?.pushViewController(controller, animated: true)
            controller.title = "Конец пути"
            controller.onBusStopSelected = {
                [weak self] busStop in
                if self?.model.startBusStop == busStop {
                    self?.swapBusStops()
                } else {
                    self?.model.endBusStop = busStop
                }
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func swapBusStops() {
        let startBusStop = model.startBusStop
        model.startBusStop = model.endBusStop
        model.endBusStop = startBusStop
    }
    
    func updateData() {
        startLabel.text = model.startBusStop?.title ?? "Начало пути"
        endLabel.text = model.endBusStop?.title ?? "Конец пути"
    }
}
