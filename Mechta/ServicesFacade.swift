import Foundation
import CoreData
import UIKit

class ServicesFacade {
    var onUpdate: (() -> Void)?
    var onError: (() -> Void)?
    var onNoNetwork: (() -> Void)?
    
    private let model: ServicesModel
    
    init() {
        self.model = AppModel.instance.servicesModel
    }
    
    func fetchedResultController() -> NSFetchedResultsController<Service> {
        return fetchedResultControllerAll()
    }
    
    private func fetchedResultControllerAll() -> NSFetchedResultsController<Service> {
        return CoreDataManager.instance.fetchedResultController(entityName: "Service", orderBy: "publishedAt")
    }
  
    func showDetailsPage(service: Service) {
        guard let detailUrl = service.detailUrl else {
            return
        }
        
        if let url = URL(string: detailUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        model.markViewed(service)
    }
    
    func callService(number: String) {
        guard let url = URL(string: "telprompt://"+number.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")) else {return}
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    var hasServices: Bool {
        return model.servicesFromStorage().count > 0
    }
    
    var unviewedServicesCount: Int {
        return model.unviewedServicesFromStorage().count
    }
    
    func updateServices() {
        model.updateServicesInStorage(onError: onError, onSuccess: onUpdateSuccess)
    }
    
    func onError(error: NetworkError) {
        switch error {
        case .fault(_): onError?()
        case .offline: onNoNetwork?()
        }
    }
    
    func onUpdateSuccess() {
        onUpdate?()
    }
}
