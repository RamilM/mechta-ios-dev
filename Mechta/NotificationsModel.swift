import Foundation
import CoreData

class NotificationsModel {
    let mainContext: NSManagedObjectContext
    let networkManager = NetworkManager()
    
    init(context: NSManagedObjectContext) {
        self.mainContext = context
    }
    
    func updateNotificationsInStorage(onError: @escaping (NetworkError) -> Void, onSuccess: @escaping () -> Void) {
        networkManager.updateItemsInStorage(
            serviceMethod: "/notifications",
            itemParser: MyNotification.from,
            entityName: "MyNotification",
            comparator: {$0 == $1},
            onError: onError,
            onSuccess: onSuccess)
    }
    
    func notificationsFromStorage() -> [MyNotification] {
        let predicate = NSPredicate(format: "hidden == %@", NSNumber(booleanLiteral: false))
        return CoreDataManager.instance.fetch("MyNotification", predicate: predicate)
    }
    
    func hide(notification: MyNotification) {
        notification.hidden = true
        try? notification.managedObjectContext?.saveIfNeeded()
        try? mainContext.saveIfNeeded()
    }
}
