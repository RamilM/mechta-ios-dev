//
//  PushActionHandler.swift
//  Mechta
//
//  Created by Evgeniy Safronov on 26.12.17.
//

import Foundation

enum PushAction {
    case showNotifications
}

class PushActionHandler {
    
    static var instance = PushActionHandler()
    
    static var pushReceivedNotification = Notification.Name(rawValue: "PushActionHandler.pushReceived")
    
    private(set) var lastAction: PushAction? = nil
    
    private init() {
    }
    
    func register(action: PushAction) {
        lastAction = action
        
        NotificationCenter.default.post(name: PushActionHandler.pushReceivedNotification, object: nil, userInfo: nil)
    }
    
    func unregister() {
        lastAction = nil
    }
}
