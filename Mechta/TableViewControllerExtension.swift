import UIKit

extension UITableViewController {
    func showLoadingBackground() {
        tableView.showLoadingBackground()
    }
    
    func showMessageBackground(_ title: String?, subtitle: String?) {
        tableView.showMessageBackground(title, subtitle: subtitle)
    }
    
    func showContentBackground() {
        tableView.showContentBackground()
    }
    
    func hideEmptyRows() {
        tableView.tableFooterView = UIView()
    }
    
    func showMessageAlert(_ message: String, title: String = "", completion:  (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: completion)
    }
}
