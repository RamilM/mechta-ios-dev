//
//  TransportNearestCell.swift
//  Mechta
//
//  Created by Evgeniy Safronov on 18.02.17.
//
//

import UIKit

class TransportNearestCell: UITableViewCell {
    @IBOutlet var startTimeLabel: UILabel?
    @IBOutlet var separatorLabel: UILabel?
    @IBOutlet var endTimeLabel: UILabel?
    @IBOutlet var leftTimeLabel: UILabel?
    @IBOutlet weak var notifyingDot: UIView!
    
    private let notificationsModel = NotificationsFacade()
    
    func show(_ item: NearestTransportItem) {
        notifyingDot.isHidden = true
        
        showTime(item)
        showNotification(item)
        showRemainingTime(item)
    }
    
    func showTime(_ item: NearestTransportItem){
        separatorLabel?.textColor = UIColor.lightGray
        endTimeLabel?.textColor = UIColor.lightGray
        startTimeLabel?.text = item.startTime.hmValue()
        endTimeLabel?.text = item.endTime.hmValue()
    }
    
    func showNotification(_ item: NearestTransportItem){
        guard let notifications = notificationsModel.getNotificationsAboutRouteFlight(routeID: item.routeID, flightID: item.flightID)
            else {
                notifyingDot.isHidden = true
                return
        }
        guard let notification = notifications.first(where: {
            $0.routeID == item.routeID && $0.flightID == item.flightID
        }) else {
            notifyingDot.isHidden = true
            return
        }
        let notiDate = notification.timeStamp != nil ? notification.timeStamp! as Date : Date()
        let datesDifference = Calendar.current.dateComponents([.day], from: notiDate, to: item.startTime).day ?? 365
        if(datesDifference < 1){
            notifyingDot.layer.cornerRadius = 3
            notifyingDot.isHidden = false;
            switch (notification.type){
            case .declinedBus :
                notifyingDot.backgroundColor = UIColor.red
            case .adjustmentBus, .delayBus, .roadSituation :
                notifyingDot.backgroundColor = UIColor.yellow
            default :
                notifyingDot.backgroundColor = UIColor.green
            }
        } else {
            notifyingDot.isHidden = true
            return
        }
    }
    
    func showRemainingTime(_ item: NearestTransportItem){
        if item.startTime.isToday {
            let seconds = lround(item.interval)
            let hours = seconds / 3600
            let minutes = (seconds % 3600) / 60
            if hours == 0 {
                leftTimeLabel?.text = "Через\n\(minutes) мин."
                if minutes < 30 {
                    leftTimeLabel?.textColor = Style.greenColor
                } else {
                    leftTimeLabel?.textColor = UIColor.darkText
                }
            } else {
                leftTimeLabel?.text = "Через\n\(hours) ч. \(minutes) мин."
                leftTimeLabel?.textColor = UIColor.lightGray
            }
            
        } else if item.startTime.isTomorrow {
            leftTimeLabel?.text = "Завтра"
            leftTimeLabel?.textColor = UIColor.lightGray
        }
    }
    
}
