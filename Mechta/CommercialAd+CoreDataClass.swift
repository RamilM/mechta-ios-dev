//
//  CommercialAd+CoreDataClass.swift
//  Mechta
//
//  Created by Ramil Minibaev on 30/08/2017.
//
//

import Foundation
import CoreData
import SwiftyJSON

@objc(CommercialAd)
public class CommercialAd: NSManagedObject {
    
    static func from(json: JSON, context: NSManagedObjectContext) -> CommercialAd {
        let commercialAd: CommercialAd = context.inserting(entityName: "CommercialAd")
        commercialAd.id = json["id"].int64!
        commercialAd.title = json["title"].string
        commercialAd.detailUrl = json["detail_url"].string
        commercialAd.body = json["body"].string
        commercialAd.priority = json["priority"].string
        commercialAd.phone = json["phone"].string
        return commercialAd
    }
    
    var photoUrl: String? {
        return photo == nil ? nil : Constants.imagesUrl + photo!
    }
}

func == (left: CommercialAd, right: CommercialAd) -> Bool {
    return left.id == right.id &&
        left.title == right.title &&
        left.detailUrl == right.detailUrl &&
        left.body == right.body &&
        left.phone == right.phone &&
        left.priority == right.priority
}
